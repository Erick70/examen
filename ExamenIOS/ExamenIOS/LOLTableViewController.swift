//
//  LOLTableViewController.swift
//  ExamenIOS
//
//  Created by Erick Cruz on 14/6/17.
//  Copyright © 2017 Erick Cruz. All rights reserved.
//

import UIKit

class LOLTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var cartTableView: UITableView!
    var lista:NSArray = []
    var selectedIndexPath:IndexPath = []
    
    
        override func viewDidLoad() {
            super.viewDidLoad()
            
            let bm = BackendManager()
            bm.getCarts()
            
            NotificationCenter.default.addObserver(self, selector: #selector(reload), name: NSNotification.Name("reload"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let bm = BackendManager()
        bm.getCarts()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }

    func reload() {
        cartTableView.reloadData()
    }
    
    func actualizarInformacion(_ notification: Notification){
        lista=notification.userInfo?["lista"] as! NSArray
        //
        cartTableView.reloadData()
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartArray.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cartCell") as! LOLTableViewCell
        cell.carta = cartArray[indexPath.row]
        cell.fillData()
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        //print(indexPath)
        selectedIndexPath = indexPath
        return indexPath
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        print(selectedIndexPath)
        
        let cart = lista[selectedIndexPath.row] as! NSDictionary
        let valor = cart["value"] as? String
        let suit = cart["suit"] as? String
        let code = cart["code"] as? String
        let destination = segue.destination as! ReadViewController
        destination.indexPath = selectedIndexPath
        
        destination.values = valor!
        destination.suit = suit!
        destination.code = code!
        
        print("segue")
    }
    
    
}
