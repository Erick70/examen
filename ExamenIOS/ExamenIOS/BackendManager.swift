//
//  BackendManager.swift
//  ExamenIOS
//
//  Created by Erick Cruz on 14/6/17.
//  Copyright © 2017 Erick Cruz. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import AlamofireImage

class BackendManager
{
    
    
    func getCarts () {
        
        let url = "https://deckofcardsapi.com/api/deck/new/draw/?count=52"
        
        Alamofire.request(url).responseObject { (response: DataResponse<CartasApiResponse>) in
            
            let cartResponse = response.result.value
            
            if let cartArray1 = cartResponse?.resultados {
                
                contador = 0
                cartArray = cartArray1
                            NotificationCenter.default.post(name: NSNotification.Name("reload"), object: nil)
                
            }
        }
    }
    
    
    func getImage(_ id:String, completionHandler: @escaping(UIImage)->()){
        
            let url = "https://deckofcardsapi.com/static/img/\(id).png"
        
            Alamofire.request(url).responseImage { response in
                if let image = response.result.value {
                    completionHandler(image)
                }
            }
        
    }

}
