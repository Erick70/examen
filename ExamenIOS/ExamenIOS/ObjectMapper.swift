//
//  ObjectMapper.swift
//  ExamenIOS
//
//  Created by Erick Cruz on 14/6/17.
//  Copyright © 2017 Erick Cruz. All rights reserved.
//

import Foundation
import ObjectMapper

class CartasApiResponse:Mappable{
    
    var resultados:[Cart]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        resultados <- map["cards"]
    }
}


class Cart:Mappable {
    
 
    var image:UIImage?
    var code:String?
    var suit:String?
    var value:String?
    
    
    
    required init(map:Map) {
        
    }
    
    func mapping(map: Map) {
        
        value <- map["value"]
        suit <- map["suit"]
        code <- map["code"]
    }
    
}
