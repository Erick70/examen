//
//  ReadViewController.swift
//  ExamenIOS
//
//  Created by Erick Cruz on 15/6/17.
//  Copyright © 2017 Erick Cruz. All rights reserved.
//

import UIKit

class ReadViewController: UIViewController {
    
  
    @IBOutlet weak var lblSuit: UILabel!
    @IBOutlet weak var lblValues: UILabel!
    @IBOutlet weak var lblDetalles: UILabel!
    
    var code:String = ""
    var values:String = ""
    var suit:String = ""
    
    var indexPath:IndexPath?
    override func viewDidLoad() {
        super.viewDidLoad()
        lblDetalles.text = code
        lblValues.text = values
        lblSuit.text = suit
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
