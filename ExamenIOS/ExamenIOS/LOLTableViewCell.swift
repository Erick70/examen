//
//  LOLTableViewCell.swift
//  ExamenIOS
//
//  Created by Erick Cruz on 14/6/17.
//  Copyright © 2017 Erick Cruz. All rights reserved.
//

import UIKit

class LOLTableViewCell: UITableViewCell {

    @IBOutlet weak var nombreCell: UILabel!
    @IBOutlet weak var imageCell: UIImageView!
    
    var carta:Cart!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func fillData(){
        
        nombreCell.text = "\(carta.value!)"
        
        if carta.image == nil {
            
            let bm = BackendManager()
            bm.getImage((carta?.code)!, completionHandler: { (imageR) in
                
                DispatchQueue.main.async {
                    self.imageCell.image = imageR
                }
                
            })
            
        } else {
            
            imageCell.image = carta.image
            
        }
        
    }

}
